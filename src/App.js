import React from 'react'
import { BrowserRouter as Router, Switch } from 'react-router-dom'
import SimpleLayout from './layouts/SimpleLayout/SimpleLayout'
import { Redirect } from 'react-router'
import { PublicRoute } from './router/PublicRoute/PublicRoute'
import HomePage from './pages/HomePage/HomePage'

function App() {
    return (
        <Router basename={`/`}>
            <Switch>
                <PublicRoute exact path={`/`} component={()=>(<Redirect to={`/home`} />)} layout={SimpleLayout}/>
                <PublicRoute path={`/home`} component={HomePage} layout={SimpleLayout}/>
            </Switch>
        </Router>
    )
}

export default App
