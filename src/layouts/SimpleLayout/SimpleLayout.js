import React, { Component } from 'react'

class SimpleLayout extends Component {
    render() {
        const Component = this.props.component
        const { location } = this.props
        const style = {
            root: {
                padding: '20px',
            }
        }
        return (
            <div style={style.root}>
                <Component location={ location }/>
            </div>
        )
    }
}

export default SimpleLayout